﻿printmonsvc-install:

 file.managed:
   - name: C:\bcfranchise\scripts\printmonsvc\nssm.exe
   - source: salt://windows/printmonsvc/nssm.exe

 file.managed:
   - name: C:\bcfranchise\scripts\printmonsvc\printmonsvc.ps1
   - source: salt://windows/printmonsvc/printmonsvc.ps1
 file.managed:
   - name: C:\bcfranchise\scripts\printmonsvc\installService.ps1
   - source: salt://windows/printmonsvc/installService.ps1
 file.managed:
   - name: C:\bcfranchise\scripts\printmonsvc\removeService.ps1
   - source: salt://windows/printmonsvc/removeService.ps1

 cmd.run:
   - name: powershell C:\bcfranchise\scripts\printmonsvc\installService.ps1
   - require: 