﻿# This service periodically polls the shared printers and checks that they are accessible via LAN IP via LPDSVC.
#
#
# Setup variables for Service
$pcName = $env:COMPUTERNAME
# Obtain the local ip address. 
$localIpAddress=((ipconfig | findstr [0-9].\.)[0]).Split()[-1]
# Setup the run time interval 
$sleepTime = 60
# Setup the Windows variable
$System32 = $env:SystemRoot+'\'+'System32'
# Obtain the error string from lpq. 
$errorCondition = lpq -S $localIpAddress -P garbagePrinter|Out-String

while($true) {
# Create an array containing shared printers. 
$sharedPrinters=Get-WmiObject -class Win32_Printer -computername $pcName|select-object -Unique Sharename
# Create an array containing service status. This periodically checks in on the essential services, and starts them if stopped.
$serviceStatuses=Get-Service lpdsvc,Spooler,printerwebserver
for ($ii=0; $ii -lt $serviceStatuses.length; $ii++) {
    if ($serviceStatuses[$ii].Status -eq "Stopped"){
    Start-Service -Name $serviceStatuses[$ii].ServiceName

    }
}

for ($i=0; $i -lt $sharedPrinters.length-1; $i++) {
	$printStat = lpq -S $localIpAddress -P $sharedPrinters[$i].Sharename|Out-String
    if ($printStat -eq $errorCondition){
    #$wshell = New-Object -ComObject Wscript.Shell
    #$contYN= $wshell.Popup(“LPD Service not responding on local adapter, Restarting Spooler and LPDSVC Subsystem",0,"Basecamp IT Baseline Printer Monitor",0x1)

    Stop-Service -Name "lpdsvc"
    Stop-Service -Name "spooler"
    Stop-Service -Name "printerwebserver"
    Remove-Item –path $System32\spool\printers\*  -Recurse -Force
    Start-Service -Name "lpdsvc"
    Start-Service -Name "spooler"
    Start-Service -Name "printerwebserver"}

}


Start-Sleep –Seconds $sleepTime
}